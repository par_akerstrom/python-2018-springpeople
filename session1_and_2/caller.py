from namespace import function1
import namespace

function1() # As we've directly imported function1() we can use it without the method.calling way
print(dir()) # Print out all functions that are available to us
namespace.function2() # Calling function2 in module namespace