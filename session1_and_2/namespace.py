# global is needed to modify values in functions
myvar = 100

def function1():
    print("myvar is:", myvar) #This is OK as it's a global variable and we're not modifying the variable
    mylocalvar = 200

def function2():
    # print(mylocalvar) # This is not OK as it's a local variable in the function1 function.
    global myvar # Making the variable global, so it can be modified in this local namespace?
    myvar += 1
    print("myvar is:", myvar)

# print("Printing locals:", locals())
# print("Printing globals", globals())
