#################################################
#                                               #
# SpringPeople Python Deep Dive                 #
# Project Magic 8 Ball                          #
# Par Akerstrom 2018                            #
#                                               #
#################################################

from time import sleep  # Import sleep from time to be able to show an in progress message
# Importing random functions for time-based and random list choices
from random import randint, random, choice


def in_progress():  # Definition of function for displaying an in progress message
    print("Working on getting you advice")  # Print a simple line
    # Set an arbitrary start value for our fake precentage counter
    variable = randint(1, 9)
    # while loop to count up to something smaller than 91 (close to 100%, but never over)
    while variable < 91:
        # Print our fake counter plus a percentage sign
        print(str(variable) + "%")
        # increase our counter with between 6-9 to make it look like we are making progress
        variable += randint(6, 9)
        # sleep the script for a random amount of time betwen 0 and 1 seconds
        sleep(random())
    print("100%\n")  # print a fake "100%"
    sleep(0.5)  # Sleep a bit more to give it a better feel
    print("Found you advice!\n")  # prints a string to the user
    sleep(random())  # Sleep a bit more to give it a better feel


def sentence_generator():  # Definition of our advice random generator
    try:  # a try block used to wrap the open file statement. If our file doesn't exist we can catch the error
        # open a file we have constructed with 10 random sentences.
        with open('random_sentences.txt') as sentences:
            sentence_list = sentences.readlines()  # read the file into a list
            # Use choice() from random module to select a random sentence from our list
            print("The Magic 8 ball says:", choice(sentence_list))
    except FileNotFoundError:  # Catch File Not Found errors
        print("Something went wrong whilst opening or reading the file random_sentences.txt")


# Initial question to the user to start the game. This always happens on startup.
input("Press enter to get advice\n")

while True:       # While loop to cycle infinite amount of advice to the user
    in_progress()  # Call our in progress function, the user has commenced the program
    # We're done displaying our fake progress bar. We now execute the sentence generator function
    sentence_generator()
    while True:  # While loop to catch user input about him/her wanting to play again or not
        # Ask the user if he wants to play
        answer = input("Do you want to play again? y/n\n")
        if answer == "y":  # if loop to check if user wants to play again
            break  # exit the loop back into the main loop that will play the game again
        elif answer == "n":  # if loop with the answer for "n"
            exit = True  # set exit variable, which will exit the main while loop
            break  # jump out of the current while loop
        else:  # Catch any answers that were not y or n
            # tell the user to try again
            print("You didn't say either \"y\" or \"n\", try again\n")
            continue  # back to the top of the current while loop
    if exit is True:  # if loop to check if we have set the exit variable
        # tell the user we're exiting.
        print("Thanks for playing, exiting...\n")
        break  # break the loop, exiting the game
