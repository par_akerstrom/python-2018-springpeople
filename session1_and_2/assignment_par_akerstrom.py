#################################################
#                                               #
# SpringPeople Python Deep Dive                 # 
# Assignment                                    #
# Par Akerstrom 2018                            #
#                                               #
#################################################

def question_printer(question_reference):
    print ("\n#################################################")
    print (question_reference)
    print ("#################################################")

#################################################
question_printer("1. Write a method which can calculate square value of number.")
#################################################

# Hard coded value for calculation. We can make it a 
# user input field if we wish to have it interactive.
number_for_calc = 4
# Save value of user_number to the power of two in new variable user_number_square
square_number = number_for_calc ** 2
# Print the two values with formatting
print("{} is the square root of {}".format(square_number,number_for_calc))

#################################################
question_printer ("2. Define a function that can convert a integer into a string and print it in console.")
#################################################

# Definition of function that will convert integer to string
def int_to_string(arg1):
    # convert the argument to an integer and print this in the console
    print("We've converted your integer into a string, your string is:", str(arg1))

# Call the function to test
int_to_string(100)

#################################################
question_printer ("3. Please write a program to shuffle and print the list [3,6,7,8].")
#################################################

# Import the ready-made function shuffle from the random module
from random import shuffle 
# Create our list
my_list = [3,6,7,8]
# Shuffle the list
shuffle(my_list)
# Print the list
print("Your list has been shuffled, it is now:", my_list)

#################################################
question_printer ("4. Please write a program which accepts a string from console and print it in reverse order.")
#################################################

# Ask user for a string input, convert to string (not necessary but we can be explicit here)
user_input_string = input(str("Please give me a string\n"))
# Create a new string and store it in the variable reversed_string. The string created uses the [start,stop,step] argument. 
# If start does not have a value it assumes 0
# If stop does not have a value it assumes total length of the string
# step -1 means start slicing from the end of the string
reversed_string = user_input_string[::-1] 
# Print the reversed string
print(reversed_string)

#################################################
question_printer ("5. Please generate a random float where the value is between 10 and 100 using Python math module")
#################################################

# Import uniform from the random module. uniform is used to create a random float between selected endpoint.
#  I couldn't figure out how to do this with the math module as the assignment suggests?
from random import uniform
# Generate a float between 10,100 using uniform function. Round this to two decimals for readability with round(). Save the result in float random_number
random_number = round(uniform(10,100),2)
# Print the number
print (random_number)

#################################################
question_printer ("6. Write a program which can compute the factorial of a given numbers. The results should be \
 printed in a comma-separated sequence on a single line. Suppose the following input is supplied \
 to the program: 8 \
 Then, the output should be: 40320")
#################################################

# We define a function that we'll use recursively (calling itself). It takes one argument.
def factorial(x):
    # if our argument is 1, we break out of the loop, the 1 is still used in the multiplication.
    if x == 1:
        return 1
    else:
        # We multiply our argument with the calling of the function itself and the argument minus 1.
        # This turns into a factorial calculation of the sense 8*7*6*5*4*3*2*1
        return x * factorial(x-1)

# Get user input and save as int so we can make calculations with the number
factorial_input = int(input("Please give me a number that I will calculate the factorial of:\n"))
# Call the recursive function factorial() with our number and print the returned result.
print ("Factorial of {} is:".format(factorial_input), factorial(factorial_input))

# NOTE: This does not handle multiple numbers as the question requested! 

#################################################
question_printer ("7. With a given integral number n, write a program to generate a dictionary that contains (i, i*i) \
 such that is an integral number between 1 and n (both included). and then the program should\
 print the dictionary. Suppose the following input is supplied to the program: 8\
 Then, the output should be:\
 {1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64}")
#################################################

# Provide an integer to the program
user_integer = 8
print("Given integer is {}".format(user_integer))
# Create an empty dictionary that we will use to store values and print our result with
output_dictionary = {}
# Convert our given integer to a list of all numbers from 1 to the given integer+1 (as directed by the question)
user_range = range(1, user_integer+1)

# Loop over the range in a for loop
for i in user_range:
    # Save each step in the range as a key value in our dictionary, save the value as the key*key
    output_dictionary[i] = i*i
# Print our result
print(output_dictionary)

#################################################
question_printer("8. Define a class which has at least two methods:\
getString: to get a string from console input\
printString: to print the string in upper case.\n\
optional: Also please include simple test function to test the class methods")
#################################################

# Define a class as described
class do_strings():
    # define method getString
    def getString(self):
        # Get an input string from the user
        input_string = input("Give me a string please:")
        # Return the string back to the caller
        return input_string
    
    # Define function printString, argument to the function is input_string, just a name coincidence to the previous input_string.
    # Nevertheless we want that input_string from the user.
    def printString(self, input_string):
        # Make it into an uppercase string by using method upper()
        upper_case_input_string = input_string.upper()
        # Return the uppercase string to the caller
        return upper_case_input_string

# Instantiate an object of do_strings
my_string_object = do_strings()
# save the result from getString in variable input_string. Call the function getString on the object my_string_object
input_string = my_string_object.getString()
# Print the result of the returned value from function printString. Give the argument input_string to the function.
print("Your string in uppercase is:", my_string_object.printString(input_string))

#################################################
question_printer("9. Define a class Person and its two child classes: Male and Female. All classes have a method\
\"getGender\" which can print \"Male\" for Male class and \"Female\" for Female class.")
#################################################

# Creating a class called Person
class Person():
    # Definition of an init function that is run at instantiation of Person objects. Only reason to create it is that the class would be empty otherwise.
    def __init__(self,name):
        # Give each person a name. We take the name as an argument to the class
        self.name = name
    
# New class that inherits from Person class. We call the class male and take no arguments to it.
class Male(Person):
    # Mandatory function getGender. Only argument is the object itself.
    def getGender(self):
        # Send back a string to the caller
        return "Male"

# New class that inherits from Person class. We call the class male and take no arguments to it.
class Female(Person):
        # Mandatory function getGender. Only argument is the object itself.
    def getGender(self):
        # Send back a string to the caller
        return "Female"

Aarav = Male("Aarav")
Aadhya = Female("Aadhya") 
print("{}'s gender is: ".format(Aarav.name), Aarav.getGender())
print("{}'s gender is: ".format(Aadhya.name), Aadhya.getGender())

#################################################
question_printer("10. Write a program that accepts a sentence and calculate the number of letters and digits. Suppose \n\
the following input is supplied to the program:\n\
hello world! 123\n\
Then, the output should be:\n\
LETTERS 10\n\
DIGITS 3")
#################################################

# Importing findall from regular expression module to be able to do regex searches 
from re import findall
# Definition of character search pattern. We do not look for unicode special characters, only aA-zZ in a raw string
characther_search_pattern = r"[a-zA-Z]"
# Definition of digits search pattern in a raw string
digit_search_pattern = r"[0-9]"
# Definition of our string we're going to search
user_input_string = "hello world! 123"
# use findall to search given string for the given search pattern (letters) and put each result into a list. Print the length of the list.
print("LETTERS", len(findall(characther_search_pattern, user_input_string)))
# use findall to search given string for the given search pattern (digits) and put each result into a list. Print the length of the list.
print("DIGITS", len(findall(digit_search_pattern, user_input_string)))

