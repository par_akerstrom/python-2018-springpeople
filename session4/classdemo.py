""" Employee class with various comments """

""" in general the classmethods are methods used to set boundaries and reduce code for 
creation of object of the class.
It can heavily reduce the code required in the call, say that you have 10 different
values that you need to pass to the class... the classmethod can help build restrictions
and make it easier for you.

staticmethods are used to provide utility functions for the class or for your whole code.
The static methods DO NOT NEED THE "self" object!
This is the key to what a staticmethod are.
It means that we don't need to create an object to use the staticmethod.
We can instead execute it straight on the class itself.
Employee.calculateTax()
"""

class Employee:

    def __init__(self,name,designation):
        """ bound/instance method of class Employee """
        self.name = name
        self.designation = designation
        self.salary = 10000

    def gettax(self):
        self.calculatetax(self.salary)

    def displaydata(self):
        return self.name,self.designation
    
    # Classmethods are helpful as you dicatate boundaries for the creation of objects.
    # You do not worry about input from the users that much. You have already defined
    # the method of how an admin should be created in the class Employee.
    @classmethod
    def createAdmin(cls,name): #this will create administrator employee object
        return cls(name,'Administrator')

    # Another classmethod to set boundaries for marktering objects of the class employee.
    @classmethod
    def createMarketing(cls,name): #this will create marketing employee object
        return cls(name,'Marketing')

    # staticmethod decorator is an auxiliary method
    @staticmethod
    def calculateTax(salary):
        return salary * 0.2


objjohn = Employee('John', 'Marketing')
objmark = Employee('Mark', 'Administrator')

objadmin1 = Employee.createAdmin('admin1')
objadmin2 = Employee.createAdmin('admin2')
objadmin3 = Employee.createAdmin('admin3')
objadmin4 = Employee.createAdmin('admin4')
objadmin5 = Employee.createAdmin('admin5')

print(objadmin1.displaydata())