#################################################
#                                               #
# SpringPeople Python Deep Dive                 # 
# Assignment Web Services                       #
# Par Akerstrom 2018                            #
#                                               #
#################################################
"""
Webservice 1.
To Store Customer data in customer.txt file
    Attribute in customer class
        customer code
        name
        Address

Webservice 2.
List all customers stored in Customer.txt file

Fetch Dog API from (https://github.com/toddmotto/public-apis) and list dogs
"""

# Import Flask and request to manage web services
from flask import Flask, request
# Import Resource and Api to build a restful api
from flask_restful import Resource, Api
# Import json to manage json files
import json

# Mandatory flask and api code cut from examples.
app = Flask(__name__)
api = Api(app)

# Class definition of root directory, it takes a Resource as argument.
class Assignment(Resource):
    # Definition of get function for root directory
    def get(self):
        # Print a simple string back to the user
        return {'about':'This is a SpringPeople assignment'}
    
    # Definition of post function for root directory
    def post(self):
        # receive json file from user and save it as "users_json"
        users_json = request.get_json()
        # Send the json back the user along with a 200 OK
        return {'you sent': users_json}, 200

# Class definition for /add/ directory, it takes a Resource as an argument
class AddEmployee(Resource):
    # Definition of a post function to receive and store the user's input
    def post(self):
        # append our employee list kept in memory with the user's input
        employees.append(request.get_json())
        # Open file stored on disk in write mode.
        # Effectively we will re-write this file every time, quite unnecessary.
        with open('users_from_client.json', mode = "w") as customers_users:
            # Dump the data found in memory in the list employees[] and put in json format in customers_users.
            # we make it human readable by having sort_keys set to true and with adding indent
            json.dump(employees, customers_users, sort_keys=True, indent=4)
        # Tell the user that the file has been updated, gives the user back the list we have in memory.
        return {'you updated the user file with this': employees}, 201

# Class definition for /show/ directory, it takes a Resource as an argument
class ShowEmployee(Resource):
    # Definition of a get function that will show the user the customer list
    def get(self):
        # Try statement as we're opening a file, we may want to manage exceptions
        try:
            # Opening our file on disk for reading
            with open('users_from_client.json') as customers:
                # load json file into variable customers
                customers = json.load(customers)
            # send the variable customers back to the user along with a 200 OK message
            return customers, 200
        # if something went wrong in the above we catch the exception
        except:
            # print a string to the console for troubleshooting
            print("Something went wrong with getting the customer file, or printing it")
            return 503

# Mandatory API code for adding URLs.
# Classes are mapped to URLs.
api.add_resource(Assignment, '/')
api.add_resource(ShowEmployee, '/show/')
api.add_resource(AddEmployee, '/add/')

# Definition of the employee list variable that is used in our AddEmployee class
employees = []

# Below code will run the session in debug mode so that we will be able
# to troubleshoot anything from the CLI
if __name__ == '__main__':
    app.run(debug=True)
