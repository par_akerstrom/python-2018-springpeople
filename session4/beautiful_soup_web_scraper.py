# $ pip install requests
# $ pip install BeautifulSoup4

from bs4 import BeautifulSoup
import urllib3

http = urllib3.PoolManager()

url = 'http://www.pythonforbeginners.com'
response = http.request('GET', url)
soup = BeautifulSoup(response.data,features="html.parser")

#print (soup.prettify)
print(soup.find_all(r"h2"))