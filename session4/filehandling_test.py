# Open and read file into variable
def read_my_file():
    with open('users_from_client.txt') as input_file:
        my_json = input_file.read()
        print("This is the contents of my file:\n", my_json, "\n")

def append_my_file():
    # Open and append text to file
    with open('users_from_client.txt', mode="a") as input_file:
        input_file.write("\nmy string that should be added")

read_my_file()
append_my_file()
read_my_file()