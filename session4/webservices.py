"""
Web services
    REST - URL based web service
        The endpoint is the URL itself.
        example: http://www.example.com/yourservice?u=1&t=2 
        Note how there are restrions on what we can pass to the web service. You're bound 
        by the URL itself. All data is passed via the URL.

        OUTPUT (XML, JSON or in CSV)
            <xml>
                <record></record>
            <xml>
    
        Application/software:
        Postman, Restlet chrome extensions to test. Interactive environment straight in chrome.
        Curl from CLI, or wget.
        
    SOAP - 
        REQUEST: Endpoing in XML format
            WSCONFIG

        RESPONSE
            XML

        Application/software:
            SOAPUI


        XML RPC -

Formats
    XML
    JSON


SOA - Service Oriented Architecture
SOA is based around you not caring about what's on the backend, you pass your arguments

Microservices - this is a subset of SOA.
"""

# Webservice development
"""
# REST 
    Four Operations (CRUD)
    create       [ POST    ]
    read (view)  [ GET     ]
    update       [ PUT     ]
    delete       [ DELETE  ]

Store data
    DB - NoSQL (SQLLite, Mongo), MySQL, Maria, Sybase
    TEXT/CSV

"""