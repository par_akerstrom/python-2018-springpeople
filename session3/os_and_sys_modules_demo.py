# os is used when we grab something from the the operating system
# This come into great play for infrastructure coding of course.

import os
import sys
import stat

# print the full list of methods in os:
print(dir(os), "\n\n")

print(os.get_exec_path(), "\n\n")

#change permission of a file to readable by all
os.chmod('changemypermission.txt', stat.S_IEXEC)
# we can also make directories, files, and whatever you wish!

#
print("your filesystem encoding is:", sys.getfilesystemencoding())