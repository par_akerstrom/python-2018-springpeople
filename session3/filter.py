# Filter function is useful if you want to filter out input

fruits = ['apple','','banana','grapes',None, 0]
print (fruits)
#filter out None objects. The filter creats a filter objects, which we need to convert to a list to be able to print
print (list(filter(None,fruits)))

def bananafilter(fruitname):
    # the trick is that this will filter return value based on a "filter". If the below is true, the value is returned. If false, the filter function will not pass the value back.
    return fruitname == 'banana'

mappedfruit = filter(bananafilter, fruits)
print(list(mappedfruit))





# If we do this just as a normal function... we get the false value rather than filtering out the value I guess... so incomplete.. but makes sense
def bananafilter2(fruitname):
    # the trick is that this will filter return value based on a "filter". If the below is true, the value is returned. If false, the filter function will not pass the value back.
    return fruitname == 'banana'

mappedfruit = bananafilter2(fruits)
print(mappedfruit)
