# With yield we're creating a compound return value

def createGenerator():
    mylist = range(4)
    for i in mylist:
        yield i*i
    
mygenerator = createGenerator()
print(list(mygenerator))
for i in mygenerator:
    print(i)