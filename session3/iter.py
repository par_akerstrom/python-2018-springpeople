# iter is a simple mechanism for extracting values one by one from an iterable

# iter creates an object which is an iterable.
# in this example we take an already iterable variable (a list!)
# and makes it into an iterable object.

schoolfriends = ['A', 'B', 'C']
myiter = iter(schoolfriends)

myfriend1 = next(myiter)
myfriend2 = next(myiter)
myfriend3 = next(myiter)

print(myfriend1)
print(myfriend2)
print(myfriend3)

for friends in schoolfriends:
    print(friends)

# below wasn't working in our demo
while next(myiter):
    print(next(myiter))
