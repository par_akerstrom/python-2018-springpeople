# used to compare files, for example when downloading something from the internet
# this didn't work in python3, so we left it as an open question

import hashlib

yourname = b"paak" # We put b in front of the string to make it a unicoded string. 
# this is required for the hashlib input.

md5 = hashlib.md5()
md5.update(yourname)
print("MD5", md5.hexdigest())

sha256 = hashlib.sha256()
sha256.update(yourname)
print("SHA256", sha256.hexdigest())

# visual studio does not seem to want to provide the available methods in the hashes...
# so we print out the available methods statically so we can see what we can use.
print(dir(md5))