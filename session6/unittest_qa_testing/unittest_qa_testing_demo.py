# unittest is a testing utility for you yourself as a developer.
# it's a simple module that provides basic layer of testing.
# We create functions that will test "Assert" functions.
# Then we execute this from the terminal
import unittest

class Calculation:
    @staticmethod #creation of a utility function
    def addition(a,b):
        return a+b

class CalculationTestcases(unittest.TestCase): #inherits from testcase
    def testadditionwithnumber(self):
        self.assertEqual(Calculation.addition(7,3),10) #The assert function is available in pretty
        # much all languages

    def testadditionwithnegativenumber(self):
        self.assertEqual(Calculation.addition(-7,3),-4) #The assert function is available in pretty
        # much all languages
    
    def testcalculationobject(self):
        myobj = Calculation()
        self.assertIsInstance(myobj,Calculation)

# run from console
# python -m unittest unittest_qa_testing_demo.CalculationTestcases.testadditionwithnumber
# python -m unittest <filename>.<Class>.<method>

# DISCLAIMER
# There are SO MANY different modules for testing, automated testing etc available.
# This is a whole field of itself, for QA. but we can do basic level testing as developers.

# How to run on a project that we've created:
# create a test folder.
# import module from parent.
# Tip is to create very modular code. Functions should only do small tasks, not mixing them up.
# functions should have very specific in their nature