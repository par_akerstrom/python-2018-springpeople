# A framework for web development
# https://bottlepy.org/docs/dev/
# pip install bottle

##### To create this in a virtual environment ######
# mkdir bottledemo
# virtualenv bottledemo
# source ./bottledemo/Scripts/activate
# pip install bottle

from bottle import route, run, template


@route('/hello/<name>')
def index(name):
    return template('<b>Hello {{name}}</b>!', name=name)

run(host='localhost', port=8080)