# supports postgresql (super popular for python), mysql, sqlite

# http://docs.peewee-orm.com/en/latest/peewee/quickstart.html

from peewee import *

db = SqliteDatabase('people.db')

class Person(Model):
    name = CharField()
    birthday = DateField()

    class Meta:
        database = db 


# Can also be used together with Flask, see this package:
# flask-peewee (3.0.3)                  - Peewee integration for flask